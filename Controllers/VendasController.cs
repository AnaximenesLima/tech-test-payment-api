﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;


namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendasContext _context;
        public VendasController(VendasContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(Vendas vendas)
        {

            _context.Add(vendas);
            _context.SaveChanges();
            return Ok(vendas);
                
        }
        [HttpGet("BuscarVenda/{id}")]
        public IActionResult BuscarVendaPorId(int id)
        {
            var vendas = _context.Vendas.Find(id);
            if (vendas == null)
            {
                return NotFound();
            }
            return Ok(vendas);
        }
        [HttpPut("AtualizarStatusVenda/{id}")]
        public IActionResult AtualizarVenda(string statusvenda, Vendas vendas)
        {
            var vendasBanco = _context.Vendas.Find(statusvenda);

            if (vendasBanco == null)
            {
                return NotFound();
            }
            vendasBanco.StatusVenda = vendas.StatusVenda;

            _context.Vendas.Update(vendasBanco);
            _context.SaveChanges();
            return Ok(vendasBanco);
            
        }
       
        [HttpDelete("{id}")]
        public IActionResult DeletarVenda(int id)
        {
            var vendasBanco = _context.Vendas.Find(id);

            if (vendasBanco == null)
            {
                return NotFound();
            }
            _context.Vendas.Remove(vendasBanco);
            _context.SaveChanges();
            return NoContent();
        }
        
    }
}

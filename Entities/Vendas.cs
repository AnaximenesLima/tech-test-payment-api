﻿namespace tech_test_payment_api.Entities
{
    public class Vendas
    {
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public string TipoPagamento { get; set; }
        public double DescontoVenda { get; set; }
        public double Valor { get; set; }
        public string Produto { get; set; }
        public string StatusVenda { get; set; }
        public DateTime DataVenda { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Context
{
    public class VendasContext : DbContext
    {
        public VendasContext(DbContextOptions<VendasContext> options) : base(options)
        {

        }

        public DbSet<Vendas> Vendas { get; set; }
        public DbSet<Vendedor> Vendedor { get; set; }
        
    }
}
